resource "openstack_dns_zone_v2" "this" {
  count = var.zone_id == "" ? 1 : 0

  name       = var.zone_name
  email      = var.zone_email
  ttl        = var.zone_ttl
  project_id = var.project_id
  type       = "PRIMARY"
}

locals {
  zone_id = var.zone_id == "" ? openstack_dns_zone_v2.this[0].id : var.zone_id

  # The record types whose values end with hostnames that might be relative
  # in the input, and which should be absolute in the output.
  hostname_value_types = toset(["NS", "CNAME", "DNAME", "MX", "PTR", "SRV"])

  # We have a tree-like architecture of recordsets, so we need to collect each
  # of those to create objects we can pass to the provider.
  recordsets = flatten([
    for name, recordsets in var.recordsets : [
      for type, recordset in recordsets : {
        name        = name == "@" ? var.zone_name : replace(name, "/([^\\.])$/", "$1.${var.zone_name}")
        description = recordset.description
        ttl         = recordset.ttl
        type        = type
        records = contains(local.hostname_value_types, type) ? [
          for record in recordset.records : record == "@" ? var.zone_name : replace(record, "/([^\\.])$/", "$1.${var.zone_name}")
        ] : recordset.records
      }
    ]
  ])
}

resource "openstack_dns_recordset_v2" "this" {
  for_each = { for rs in local.recordsets : "${rs.name}-${rs.type}" => rs }

  zone_id    = local.zone_id
  project_id = var.project_id

  name        = each.value.name
  description = each.value.description
  ttl         = each.value.ttl
  type        = each.value.type
  records     = each.value.records
}

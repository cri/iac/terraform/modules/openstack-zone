# Terraform OpenStack Zone Module

Terraform module which creates DNS zones and records.

## Usage

There are two ways of using this module.

##### Existing DNS zone

```hcl
resource "openstack_dns_zone_v2" "example_org" {
  name        = "example.org."
  email       = "jdoe@example.org"
  description = "An example zone"
  ttl         = 3600
  type        = "PRIMARY"
}

module "dns_example_org" {
  source = "git::https://gitlab.cri.epita.fr/cri/iac/terraform/modules/openstack-zone"

  zone_id     = openstack_dns_zone_v2.example_org.id
  zone_name   = openstack_dns_zone_v2.example_org.name

  recordsets = {
    "@" = {
      MX = {
        records = ["5 mail", "10 backup.mail.example.com."]
        description = "Mailservers, one in our zone, another one in someone else's"
        ttl = 3600
      }
    }
    mail = {
      A = { records = ["192.0.2.42"] }
    }
  }
}
```

##### New DNS zone

```hcl
module "dns_example_org" {
  source = "git::https://gitlab.cri.epita.fr/cri/iac/terraform/modules/openstack-zone"

  zone_name = "example.org."

  recordsets = {
    "@" = {
      A = { records = ["192.0.2.42"] }
    }
    www = {
      CNAME = { records = [""] } # this record will automatically get expanded
    }
  }
}
```

## License

As a license has not yet been chosen for this project, all rights are reserved.

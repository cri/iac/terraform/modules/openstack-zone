output "zone_id" {
  value = local.zone_id
}

output "recordsets" {
  value = local.recordsets
}

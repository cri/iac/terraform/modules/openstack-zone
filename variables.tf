variable "zone_id" {
  type        = string
  default     = ""
  description = <<-EOD
    Id of the zone where records will be created. Optional, in which case the
    zone will be created.
  EOD
}

variable "zone_email" {
  type        = string
  default     = ""
  description = <<-EOD
    Hostmaster email. Optional in case the zone already exists.
  EOD
}

variable "zone_ttl" {
  type        = number
  default     = 3600
  description = <<-EOD
    Default ttl of the zone. Optional in case the zone already exists.
  EOD
}

variable "zone_name" {
  type        = string
  description = "Name of the zone where records will be created."

  validation {
    condition     = can(regex("\\.$", var.zone_name))
    error_message = "The zone_name must end with a . character."
  }
}

variable "project_id" {
  type        = string
  default     = ""
  description = "The ID of the project DNS zone and recorsets are created for."
}

variable "recordsets" {
  type = map(map(object({
    records     = set(string)
    description = optional(string)
    ttl         = optional(string)
  })))
  description = "Set of DNS records objects to manage."
}
